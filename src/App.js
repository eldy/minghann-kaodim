import React, { Component } from 'react'
import { Card } from 'reactstrap'
import Header from './components/Header'
import Question from './components/Question'
import Footer from './components/Footer'
import cx from 'classnames'
import styles from './App.module.scss'
import validateAnswer from './validateAnswer'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: true,
      selectedQuestion: 1
    }
  }
  ref = React.createRef()

  componentDidMount() {
    //assume payload from api
    const payload = {
      title: 'Wedding Service',
      questions: [
        {
          id: 2447,
          question_type: 'singleTextQuestion',
          prompt: 'What is your location?',
          placeholder: 'eg. Kuala Lumpur',
          is_required: true,
          min_char_length: 5
        },
        {
          id: 2500,
          question_type: 'MultipleChoice',
          prompt: 'What service do you need?',
          promptOptions: [
            { label: 'Photography', value: 'photography' },
            { label: 'Videography', value: 'Videography' },
            { label: 'Event', value: 'event' },
            { label: 'Other', value: '' }
          ],
          multipleChoiceExtraPlaceholder: 'Please specify',
          is_required: true
        },
        {
          id: 2448,
          question_type: 'multipleTextQuestion',
          prompt: "Describe in short what is the problem you're facing.",
          is_required: true,
          min_char_length: 10
        }
      ]
    }

    const { title, questions } = payload
    this.setState({
      title,
      questions,
      loading: false,
      submitting: false
    })
  }

  inputRefs = []
  genRef = ref => {
    this.inputRefs.push(ref)
  }

  sendValidateErrors = errors => {
    const { selectedQuestion, questions } = this.state
    const index = selectedQuestion - 1

    this.setState(
      {
        questions: questions.map(
          (q, i) =>
            i === index
              ? {
                  ...q,
                  validationErrors: errors
                }
              : q
        )
      },
      () => {
        this.inputRefs[index].focus()
      }
    )
  }

  onChangeQuestion = type => {
    const { selectedQuestion, questions } = this.state
    const targetedQuestion = questions[selectedQuestion - 1]

    if (selectedQuestion <= questions.length && type === 'next') {
      validateAnswer(
        targetedQuestion.is_required,
        targetedQuestion.min_char_length,
        targetedQuestion.answer,
        ({ errors }) => {
          if (errors) {
            this.sendValidateErrors(errors)
          } else {
            if (selectedQuestion === questions.length) {
              this.submitAnswers()
            } else {
              this.setState({ selectedQuestion: selectedQuestion + 1 })
            }
          }
        }
      )
    } else if (type === 'prev') {
      this.setState({ selectedQuestion: selectedQuestion - 1 })
    }
  }

  submitAnswers = () => {
    this.setState(
      {
        submitting: true
      },
      () => {
        setTimeout(() => {
          alert(
            JSON.stringify(
              this.state.questions.map(q => ({ id: q.id, answer: q.answer })),
              null,
              1
            )
          )
        }, 500)
      }
    )
  }

  onChangeAnswer = ({ e, id, multipleChoice, multipleChoiceExtraValue }) => {
    const { questions } = this.state

    let answer
    if (multipleChoice && !multipleChoiceExtraValue) {
      answer = e
    } else if (multipleChoice && multipleChoiceExtraValue) {
      answer = multipleChoiceExtraValue
    } else {
      answer = e.target.value
    }

    this.setState({
      questions: questions.map(q => {
        if (q.id === id) {
          console.log('wtf')
          return {
            ...q,
            answer,
            validationErrors: [], //don't keep validation error when user is typing,
            openMultipleChoiceExtra: multipleChoice && e === ''
          }
        } else {
          return { ...q }
        }
      })
    })
  }

  render() {
    const {
      loading,
      selectedQuestion,
      title,
      questions,
      submitting
    } = this.state

    if (loading) return '...loading'

    return (
      <div className={cx('mx-auto', styles.formWrap)}>
        <Card>
          <Header
            title={title}
            selectedQuestion={selectedQuestion}
            questions={questions}
            submitting={submitting}
          />
          <hr />
          {questions.map((props, index) => (
            <Question
              id={props.id}
              questionIndex={index + 1}
              selectedQuestion={selectedQuestion}
              onChangeAnswer={this.onChangeAnswer}
              questionType={props.question_type}
              ref={this.genRef}
              {...props}
            />
          ))}
          <Footer
            onChangeQuestion={this.onChangeQuestion}
            selectedQuestion={selectedQuestion}
            validationErrors={questions[selectedQuestion - 1].validationErrors}
            submitting={submitting}
          />
        </Card>
      </div>
    )
  }
}

export default App
