const validateAnswer = (isRequired, minChar, answer, cb) => {
  console.log(answer)

  let errors = []
  if (isRequired && !answer) {
    errors = ['Please complete the required field.']
  }
  if (answer && answer.length < minChar) {
    errors = [...errors, `Please type a mininum of ${minChar} characters.`]
  }
  errors.length ? cb({ errors }) : cb({ errors: null })
}
export default validateAnswer
