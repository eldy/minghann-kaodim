import React from 'react'
import styles from './Header.module.scss'
import cx from 'classnames'
import { Progress } from 'react-sweet-progress'
import 'react-sweet-progress/lib/style.css'

const Header = ({ title, selectedQuestion, questions, submitting }) => (
  <div className={'text-center pt-3 pt-3'}>
    <p className={cx('mb-0', styles.labelText)}>Submit Request for</p>
    <p className={styles.title}>{title}</p>
    <Progress
      percent={
        submitting ? 100 : ((selectedQuestion - 1) / questions.length) * 100
      }
      className={styles.progress}
    />
  </div>
)
export default Header
