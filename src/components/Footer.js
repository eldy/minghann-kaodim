import React from 'react'
import { Button } from 'reactstrap'
import styles from './Footer.module.scss'
import cx from 'classnames'

const Footer = ({
  onChangeQuestion,
  selectedQuestion,
  validationErrors = [],
  submitting
}) => (
  <div>
    <div className={cx('d-flex justify-content-between', styles.wrap)}>
      {selectedQuestion !== 1 && (
        <div
          className={styles.backBtn}
          onClick={() => onChangeQuestion('prev')}
        >
          <i className="fa fa-arrow-left" />
        </div>
      )}
      <div className={styles.estimation}>
        <div>
          <span>Estimated</span> (hourly)
          <p className={styles.price}>RM0</p>
        </div>
      </div>
      <Button
        disabled={validationErrors.length || submitting}
        onClick={() => onChangeQuestion('next')}
        className={cx('ml-auto', styles.nextBtn)}
      >
        {submitting ? 'Submitting' : 'Next'}
      </Button>
    </div>
  </div>
)

export default Footer
