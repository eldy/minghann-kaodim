import React from 'react'
import { Input, Label, FormGroup } from 'reactstrap'
import styles from './Question.module.scss'
import classNames from 'classnames/bind'
import { isMobile } from 'react-device-detect'

//css module for classnames
let cx = classNames.bind(styles)

const Question = React.forwardRef(
  (
    {
      id,
      prompt,
      is_required,
      validationErrors = [],
      placeholder,
      multipleChoiceExtraPlaceholder,
      questionIndex,
      selectedQuestion,
      questionType,
      openMultipleChoiceExtra,
      promptOptions,
      onChangeAnswer,
      answer
    },
    ref
  ) => {
    let input = (
      <Input
        type="text"
        id={id}
        name={id}
        value={answer}
        placeholder={placeholder}
        onChange={e => onChangeAnswer({ e, id })}
        innerRef={ref}
      />
    )
    if (questionType === 'MultipleChoice') {
      input = (
        <div>
          {promptOptions.map(option => (
            <FormGroup className="mb-0">
              <Label>
                <Input
                  type="radio"
                  name={id}
                  value={answer}
                  onChange={e =>
                    onChangeAnswer({
                      e: option.value,
                      id,
                      multipleChoice: true
                    })
                  }
                />
                {option.label}
              </Label>
            </FormGroup>
          ))}
          <Input
            type="text"
            id={id}
            name={id}
            placeholder={multipleChoiceExtraPlaceholder}
            className={cx({
              multipleChoiceExtraInput: true,
              show: openMultipleChoiceExtra
            })}
            onChange={e =>
              onChangeAnswer({
                e: '',
                id,
                multipleChoice: true,
                multipleChoiceExtraValue: e.target.value
              })
            }
            innerRef={ref}
          />
        </div>
      )
    } else if (questionType === 'multipleTextQuestion') {
      input = (
        <Input
          type="textarea"
          name={id}
          rows={isMobile ? 3 : 5}
          id={id}
          value={answer}
          placeholder={placeholder}
          onChange={e => onChangeAnswer({ e, id })}
          innerRef={ref}
        />
      )
    }
    return (
      <div
        className={cx({
          wrap: true,
          show: questionIndex === selectedQuestion
        })}
      >
        <h4>{prompt}</h4>
        {input}
        {is_required &&
          validationErrors.length === 0 && (
            <p className={`${styles.required} ${styles.label}`}>
              <span>*</span>
              Required
            </p>
          )}
        {validationErrors.map(error => (
          <div>
            <p className={`${styles.error} ${styles.label}`}>{error}</p>
          </div>
        ))}
      </div>
    )
  }
)

export default Question
